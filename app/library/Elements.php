<?php

use Phalcon\Mvc\User\Component;

class Elements extends Component
{

    public function setClassTodayClass($day, $type, $class)
    {
        $param = $this->view->getParamsToView();

        if($param['params']['year'] == $param['params']['today']['Year'] &&
            $param['params']['mouth'] == $param['params']['today']['month'] &&
            $day == $param['params']['today']['day'])
        {
            echo "{$type}='{$class}'";
            return;
        }

    }

    public function getButton($day)
    {
        $param = $this->view->getParamsToView();

        if($param['params']['year'] == $param['params']['today']['Year'] &&
            $param['params']['mouth'] == $param['params']['today']['month'] &&
            $day == $param['params']['today']['day'])
        {

           // print_arr($param['auth']['auth_work'][$day]);
            if(isset($param['auth']['auth_work'][$day])) {
                if($param['auth']['auth_work'][$day]['stop_time'] == NULL) {
                    echo "<button id='start-work' name='stop' type=\"button\" class=\"btn btn-danger\">Стоп</button>";
                    return;
                }
                if(!empty($param['auth']['auth_work'][$day]['stop_time']) && !empty($param['auth']['auth_work'][$day]['start_time'])) {
                    echo "<button id='start-work' name='save' type=\"button\" class=\"btn btn-success\">Старт</button>";
                    return;
                }
               // print_arr($param['params']['auth_work'][$day]['start_time']);
            }
            else{
                echo "<div id=\"work-time\"></div>";
                echo "<div id=\"pause-time\"></div>";
                echo "<button id='start-work' name='save' type=\"button\" class=\"btn btn-success\">Старт</button>";
                return;
            }

        }
    }

    public function getAdminLogin(){
        $auth = $this->session->get('auth');
        echo $auth['login'];
    }

    public function getUrlHome()
    {
        $auth = $this->session->get('auth');

        if($auth['Type'] == 'A'){
            echo '/admin/index';
        }
        else{
            echo '/home';
        }
    }
}