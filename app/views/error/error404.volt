{{ stylesheet_link('/public/css/error/404.css') }}

<section id="wrapper" class="container-fluid">
    <div class="error-box">
        <div class="error-body text-center">
            <h1 class="text-danger">404</h1>
            <h3>Страница не найдена</h3>
            <a href="{{ elements.getUrlHome() }}" class="btn btn-danger btn-rounded m-b-40">Назад на главную</a> </div>
    </div>
</section>