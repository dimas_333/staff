<div class="head-info">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h3>Вы ввошли как
                    <span class="badge badge-secondary btn btn-primary" data-toggle="modal" data-target="#exampleModalLong" >
                        {{ auth['login'] }}
                    </span>
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <p><strong>Отработано часов:</strong> {{ auth['mouth_time'] }} </p>
                <p><strong>Кол-во часов в месяц:</strong> {{ params['work_time'] }} ч</p>
                <p><strong>Опозданий на работу: </strong> {{ auth['tardiness'] }}</p>
                {{ form('index/index') }}
                    <div class="row">
                        <div class="col-6">
                            {{ form.render('year', ['class' : 'form-control year', 'value' : params['year']]) }}
                        </div>
                        <div class="col-6">
                            {{ form.render('mouth', ['class' : 'form-control mouth', 'value' : params['mouth']]) }}
                        </div>
                    </div>
                    {{ form.render('submit', ['class' : 'd-none set-work']) }}
                </form>
            </div>
        </div>
    </div>
</div>

<div class="content-info">
    <table class="table table-bordered">
        <thead>
                <th><a href="#" class="hide-table">Скрыть/раскрыть</a></th>
                <th>{{ auth['name'] }}</th>
            {% for user in users%}
                <th>{{ user['name'] }}</th>
            {% endfor %}
        </thead>
        <tbody>
             {% for param in params['all_work_day'] %}
                 <tr {{ elements.setClassTodayClass(param['number'], 'class', 'today') }} class="hide">

                     <td class="text-center align-middle">
                         {{ param['number'] }}
                         <br />
                         {{ param['day'] }}
                     </td>

                     {# Вывод рабочих часов авторизованного пользователя #}
                     <td class="text-center">
                         {% include "/blocks/AuthWorkTime.volt" %}

                         {{ elements.getButton(param['number']) }}
                     </td>

                     {# Вывод пользователей сотрудников#}
                     {% include "blocks/UserWorks.volt" %}
                 </tr>
             {% endfor %}
        </tbody>
    </table>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">
                    Инфо пользователя: {{ auth['name'] }}
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label for="xample-text-input" class="col-4 col-form-label text-right"> Старый пароль</label>
                    <div class="col-8">
                        <input type="password" id='old-pass' class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="xample-text-input" class="col-4 col-form-label text-right"> Новый пароль</label>
                    <div class="col-8">
                        <input type="password" id="new-pass" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-primary change">Созранить</button>
            </div>
        </div>
    </div>
</div>


{{ javascript_include('/public/js/works.js') }}
{{ javascript_include('/public/js/change.js') }}

{{ javascript_include('/public/js/index/index.js') }}