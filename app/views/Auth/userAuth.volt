{{ stylesheet_link('/public/css/auth/user.css') }}

{{ flash.output() }}
{{ form('/login', 'id': 'slick-login') }}
    {{ form.render('hash', ['value': security.getSessionToken()]) }}
    {{ form.render('login', ['name': 'login', 'class': 'placeholder', 'placeholder':'Логин']) }}
    {{ form.render('password', [ 'name': 'password', 'class': 'placeholder', 'placeholder':'Пароль']) }}
    {{ form.render('submit', ['value': 'Войти']) }}
</form>