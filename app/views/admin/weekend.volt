{{ stylesheet_link('/public/css/admin/weekend.css') }}
<div class="head-info">
    <h3>Список выходных</h3>
    {{ form('admin/weekend') }}
    <div class="row">
        <div class="col-6">
            {{ form.render('year', ['class' : 'form-control year', 'value' : params['year']]) }}
        </div>
        <div class="col-6">
            {{ form.render('mouth', ['class' : 'form-control mouth', 'value' : params['mouth']]) }}
        </div>
    </div>
    {{ form.render('submit', ['class' : 'd-none set-work']) }}
    </form>
</div>

<div class="content-info">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h5>Календарь  <a href="#" class="calendar-hide"><i class="fa fa-plus" aria-hidden="true"></i></a></h5>
            </div>
        </div>
    </div>
    <table class="table table-bordered table-calendar">
        <thead>
            <th>Понедельник</th>
            <th>Вторник</th>
            <th>Среда</th>
            <th>Четверг</th>
            <th>Пятница</th>
            <th>Суббота</th>
            <th>Воскресенье</th>
        </thead>
        <tbody>
        {% for day in params['calendar'] %}
            <tr>
                {% for i in 1..7 %}
                    {% if day[i] is defined %}
                        <td {{ elements.setClassTodayClass(day[i], 'class', 'btn-primary') }} class="day" name="{{ day[i] }}" data-toggle="modal" data-target="#exampleModal">{{ day[i] }}</td>
                    {% else %}
                        <td> </td>
                    {% endif %}
                {% endfor %}
            </tr>
        {% endfor %}
        </tbody>
    </table>
    <div class="list-weekend">
        <div class="container-fluid">
            <div class="row">
                <div class="col-6"><h5>Список праздников  <a href="#" class="weekends-hide"><i class="fa fa-plus" aria-hidden="true"></i></a></h5></div>
                <div class="col-6 text-right">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                        Добавить
                    </button>
                </div>
            </div>
        </div>
        <table class="table table-striped table-weekend">
            <thead>
                <th>Дата</th>
                <th>Название</th>
                <th>Повторять</th>
                <th> </th>
            </thead>
            <tbody>
            {% if weekends is defined %}
                {% for weekend in weekends %}
                    <tr>
                        <td>{{ weekend['date'] }}</td>
                        <td>{{ weekend['name'] }}</td>
                        <td>
                            {% if weekend['rep'] == 'Y' %}
                                    да
                            {% else %}
                                    нет
                            {% endif %}
                        </td>
                        <td><a href="{{ url('admin/weekenddelete/' ~weekend['id'] ) }}">Удалить</a></td>
                    </tr>
                {% endfor %}
            {% else %}
                <tr>
                    <td colspan="4" class="text-center"> Праздники отсутсвуют</td>
                </tr>
            {% endif %}
            </tbody>
        </table>
    </div>
</div>


<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Добавить праздник</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="example-date-input" class="col-2 col-form-label">Дата</label>
                        <div class="col-10">
                            <input class="form-control" type="date" value="" id="example-date-input">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-2 col-form-label">Название</label>
                        <div class="col-10">
                            <input class="form-control" type="text" value="" id="example-text-input">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-2 col-form-label">Повторять</label>
                        <div class="col-10">
                            <select class="form-control" id="example-text-select">
                                <option value="Y">Да</option>
                                <option value="N">Нет</option>
                            </select>
                        </div>
                    </div>
                </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-primary add-weekend">Добавить</button>
            </div>
        </div>
    </div>
</div>

{{ javascript_include('/public/js/works.js') }}

{{ javascript_include('/public/js/admin/weekend.js') }}