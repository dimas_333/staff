{{ stylesheet_link('/public/css/index/style.css') }}
<div class="container-fluid nav-header">
    <div class="row">
        <div class="col-6 align-self-center"><a class="navbar-brand">Мое рабочее время</a></div>
        <div class="col-6 text-right  align-self-center">{{ link_to('/exit', 'Выйти', 'class' : 'text-center link-exit') }}</div>
    </div>
</div>
{{ flash.output() }}
<main role="main" class="container">
    <div class="content-body">
    {{ content() }}
    </div>
</main>
<footer class="footer">
    <div class="container">
        <span class="text-muted">Place sticky footer content here.</span>
    </div>
</footer>
