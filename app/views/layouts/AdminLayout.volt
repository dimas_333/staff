{{ stylesheet_link('/public/css/admin/index.css') }}
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand">Админ панель</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                {{ link_to('admin/index', 'Главная', 'class' : 'nav-link') }}
            </li>
            <li class="nav-item active">
                {{ link_to('/home', 'Рабочее время', 'class' : 'nav-link') }}
            </li>
            <li class="nav-item dropdown">
                {{ link_to('#', 'Пользователи', 'id' : 'navbarDropdown', 'class' : 'nav-link', 'role' : 'button', 'data-toggle' :'dropdown', 'aria-haspopup' : 'true', 'aria-expanded' : 'false' ) }}
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    {{ link_to('admin/listuser', 'Список всех работников', 'class' : 'dropdown-item') }}
                    {{ link_to('admin/adduser', 'Добавить работника', 'class' : 'dropdown-item') }}
                </div>
            </li>
            <li class="nav-item">
                {{ link_to('admin/weekend', 'Выходные', 'class' : 'nav-link') }}
            </li>
            <li class="nav-item">
                {{ link_to('admin/settings', 'Настройки', 'class' : 'nav-link') }}
            </li>
        </ul>
    </div>
    <div class="form-inline my-2 my-lg-0">
        <ul class="nav nav-pills">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"> Вы вошли как: {{ elements.getAdminLogin() }}</a>
                <div class="dropdown-menu">
                    {{ link_to('/exit', 'Выйти', 'class' : 'dropdown-item') }}
                </div>
            </li>
        </ul>
    </div>
</nav>
{{ flash.output() }}
<main role="main" class="container">
    <div class="content-body">
        {{ content() }}
    </div>
</main>
<footer class="footer">
    <div class="container">
        <span class="text-muted">Place sticky footer content here.</span>
    </div>
</footer>