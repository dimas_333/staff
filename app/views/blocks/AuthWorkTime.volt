{% if auth['auth_work'][param['number']] is defined %}
    <div class="work-time">
        <label {{ elements.setClassTodayClass(param['number'], 'id', 'start_time') }} class="start_time">
                {{ auth['auth_work'][param['number']]['start_time'] }}
        </label>
        -
        <label {{ elements.setClassTodayClass(param['number'], 'id', 'stop_time') }} class="stop_time">
            {{ auth['auth_work'][param['number']]['stop_time'] }}
        </label>
    </div>
    <div {{ elements.setClassTodayClass(param['number'], 'id', 'pause-time') }} class="pause-time">
       {% if auth['auth_work'][param['number']]['pause'] is defined %}
             Пауза:  {{ auth['auth_work'][param['number']]['pause'] }}
       {% endif %}
    </div>
    <div class="day-work-time">
        {% if auth['auth_work'][param['number']]['all_time'] is defined %}
           Отработано:  {{ auth['auth_work'][param['number']]['all_time'] }}
        {% endif %}
    </div>
{% else %}
    <div class="work-time">

        <label {{ elements.setClassTodayClass(param['number'], 'id', 'start_time') }} class="start_time">

        </label>
        -
        <label {{ elements.setClassTodayClass(param['number'], 'id', 'stop_time') }} class="stop_time">

        </label>
    </div>
    <div {{ elements.setClassTodayClass(param['number'], 'id', 'pause-time') }} class="pause-time">

    </div>
{% endif %}