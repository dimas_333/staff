{% for user in users%}
    <td class="text-center">
        {% if user['work'][param['number']] is defined %}
            <div class="work-time">
                {{ user['work'][param['number']]['start_time'] }}
                -
                {{ user['work'][param['number']]['stop_time'] }}
            </div>
            <div class="pause-time">
                {% if user['work'][param['number']]['pause'] is defined %}
                       Пауза: {{ user['work'][param['number']]['pause'] }}
                {% endif %}
            </div>
            <div class="day-work-time">
                {% if user['work'][param['number']]['day_time'] is defined %}
                    Отработано : {{ user['work'][param['number']]['day_time'] }}
                {% endif %}
            </div>
        {% endif %}
    </td>
{% endfor %}