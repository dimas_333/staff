<?php

use Phalcon\Mvc\Model;

class settings extends Model
{




    public function setSettings()
    {
        $name_project = $this->getDI()->getRequest()->getPost('project_name');
        $work_time = $this->getDI()->getRequest()->getPost('work-time');
        $start_work_time = $this->getDI()->getRequest()->getPost('start-work-time');


        $project_name = parent::findFirst(['name = \'project-name\'']);
        $time_work = parent::findFirst(['name = \'work-time\'']);
        $time_work_start= parent::findFirst(['name = \'start-work-time\'']);

        $project_name->value = $name_project;
        $time_work->value = $work_time;
        $time_work_start->value = $start_work_time;



        if(!$project_name->save() || !$time_work->save() || !$time_work_start->save()){
            $this->getDI()->getFlash()->error('Произошла ошибка');

            return false;
        }

        $this->getDI()->getFlash()->success('Данные успешно сохранены');
    }

    public function getSettings()
    {
        $setting = parent::find();

        $setting = $setting->toArray();
        foreach($setting as $key => $value){
            if($value['name'] == 'project-name'){
                $result['project-name'] = $setting[$key];
            }
            if($value['name'] == 'work-time'){
                $result['work-time'] = $setting[$key];
            }
            if($value['name'] == 'start-work-time'){
                $result['start-work-time'] = $setting[$key];
            }
        }

        return $result;
    }
}