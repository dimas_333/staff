<?php

use Phalcon\Mvc\Model;

class weekend extends Model
{


    public function getCaledar($year = NULL, $mouth = NULL)
    {
        $result = [];

        if($year == NULL){
            $year = date('Y');
        }
        if($mouth == NULL){
            $mouth = date('m');
        }

        $result['year'] = $year;
        $result['mouth'] = $mouth;
        $works = new works();

        $result['today'] = $works->getToday();

        //Узнаем кол-во дней в месяце
        $all_day = cal_days_in_month(CAL_GREGORIAN, $mouth ,  $year);

        $week = 1;

        for($i = 1; $all_day >= $i; $i++) {

            $d = strftime('%u', strtotime($year . '-' . $mouth . '-' . $i));

            if ($d == '1') {
                $week++;
            }

            $result['calendar'][$week][$d] = $i;
        }

        return $result;
    }

    public function getWeekends()
    {
        $result = [];

        $weekend = parent::find();
        $weekend = $weekend->toArray();

        foreach ($weekend as $key => $value){
            $result[$value['id']] = $weekend[$key];
        }

       return $result;
    }


    /**
     * Метод проверят день либо сохраняет праздник
     *
     */
    public function setWeekend()
    {
        $result = ['status' => 'false'];

        $date = $this->getDI()->getRequest()->getPost('date');
        $name = $this->getDI()->getRequest()->getPost('name');
        $rep = $this->getDI()->getRequest()->getPost('rep');

        if(!empty($date) && !empty($name) && !empty($rep)){
            $this->date = $date;
            $this->name = $name;
            $this->rep = $rep;

            if($this->save())
            {
                $weekend = parent::findFirst(['date = :date: AND name = :name:', 'bind' => ['name' => $name, 'date' => $date]]);
                $result = ['status' => 'true', 'weekend' => $weekend->toArray()];
            }
        }
        return $result;
    }

    public function deleteWeekend($id)
    {
        if(intval($id['0']))
        {
            $weekend = parent::findFirst($id['0']);

            if($weekend) {

                if ($weekend->delete()) {
                    return true;
                }
            }
        }

        return false;
    }
}