<?php

use Phalcon\Mvc\Model;

class users extends Model
{

    //Добавляет или обновляет пользователя
    public function SaveUser($user = array())
    {
        if(empty($user)) {
            return $this->getDI()->getDispatcher()->forward([
                'controller' => 'error',
                'action'     => 'show401'
            ]);
        }

        $isset_user = parent::findFirst([
            'login = :login:',
            'bind' => ['login' => $user['login']]
        ]);

        if($user['user-id'] == 0) {

            //проверим если логин уже занят
            if($isset_user == 'false'){
                $this->getDI()->getFlash()->error('Данный '. $user['login'] .' логин уже занят');
                return false;
            }
            $user['password'] = $this->getDI()->getSecurity()->hash(sha1($user['password']));
        }
        else {
            $this->id = $user['user-id'];

            if($user['password'] != $isset_user->password)
            {
                $user['password'] = $this->getDI()->getSecurity()->hash(sha1($user['password']));
            }
        }
        //print_die($user['password']);


        $this->login   = $user['login'];
        $this->name    = $user['name'];
        $this->email   = $user['email'];
        $this->type    = $user['type'];
        $this->status  = $user['status'];
        $this->password = $user['password'];

        if($this->save())
            return true;
        else
            return false;

    }


    /**
     *  Метод авторизует пользователя
     *
     * @param $type
     * @return bool
     */
    public function CheckUser($type)
    {


        $login = $this->getDI()->getRequest()->getPost('login', 'string');

        $password = sha1($this->getDI()->getRequest()->getPost('password', 'string'));



        $user = parent::findFirst([
            'login = :login: AND status = :status:',
            'bind' => ['login' => $login, 'status' => 'Y']
        ]);

       // print_die($user->toArray());
        //Если пользователь сущевствует проверим пароль
        if ($user) {

            if($user->type == $type){
                if ($this->getDI()->getSecurity()->checkHash($password, $user->password) == true) {

                    $this->getDI()->getSession()->set('auth', [
                        'user_id' => $user->id,
                        'login' => $user->login,
                        'Type' => $user->type
                    ]);

                    return true;
                }
            }
            else{
                $this->getDI()->getFlash()->error('Такого пользователя не сущевствует');
            }
        } else {
            $this->getDI()->getFlash()->error('Такого пользователя не сущевствует');
        }
    }

    public function changePassword()
    {
        $auth = $this->getDI()->getSession()->get('auth');

        $old_pass = sha1($this->getDI()->getRequest()->getPost('old_pass'));
        $new_pass = sha1($this->getDI()->getRequest()->getPost('new_pass'));

        $user = parent::findFirst(['id = :id:', 'bind' => ['id' => $auth['user_id']]]);

        if($user)
        {
            if($this->getDI()->getSecurity()->checkHash($old_pass, $user->password)){
                $user->password = $this->getDI()->getSecurity()->hash($new_pass);

                if($user->save()){
                    return true;
                }
                else{
                    return false;
                }
            }
            else{
                return false;
            }
        }
        else{
            echo false;
        }
    }

}