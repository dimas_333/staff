<?php

use Phalcon\Mvc\Model;

class works extends Model
{

    /**
     *  Перебор даты для отсеивания ненужных 0
     *
     * @param $day - дата
     * @return mixed День 1 2 3 4 5
     */
    public function dayReplace($day)
    {
        $day = explode('-', $day);

        if($day['2'] != '20' && $day['2'] != '30') {
            $day['2'] = str_replace('0', '', $day['2']);
        }

        return $day['2'];
    }

    /**
     *  Получает из БД дневное рабочее время
     *
     * @return Model
     */
    public function getSettingWorkTime()
    {
        return $time = settings::findFirst([
                'name = :name:',
                'bind' => ['name' => 'work-time']
        ]);
    }

    /**
     *  Получиня даты сегодня
     *
     * @param bool $explod - True разбить на массив False - вернуть время
     * @return false|string
     */
    public function getToday($explod = TRUE)
    {
        $date = date('Y-m-d');
        if($explod){
            $date = explode('-', $date);

            list($result['Year'], $result['month'], $result['day']) = $date;
        }
        else{
            $result = $date;
        }

        return $result;
    }


    /**
     *  метод складывает время за месяц работы
     *
     * @param $time
     * @param $time2
     * @return string
     */
    public function getMouthTimeWork($time, $time2)
    {
        $time = explode(":", $time);
        $time2 = explode(":", $time2);

        $hour = 0;
        $min = 0;
        $sek = 0;

        $hour = $time['0'] + $time2['0'];

        $sek = $time['2'] + $time2['2'];

        $min = $time['1'] + $time2['1'];

        while ($sek > 59) {
            $sek -= 60;
            $min++;
        }


        while($min > 59){
            $min-=60;
            $hour++;
        }

        if(mb_strlen($sek) == 1){
            $sek = '0'.$sek;
        }
        if(mb_strlen($min) == 1){
            $min = "0".$min;
        }
        return $hour.':'.$min.':'.$sek;
    }

    /**
     * Складывает время за день
     *
     * @param $time1
     * @param $time2
     * @param bool $expression - true сложить False отнять
     * @return false|string
     */
    public function AddTime($time1, $time2 , $expression = TRUE)
    {
        $secs = strtotime($time2) - strtotime("00:00:00");
        if($expression) {
            return date("H:i:s",strtotime($time1)+$secs);
        }
        else {
            return date("H:i:s",strtotime($time1)-$secs);
        }
    }


    /**
     *  получаем разницу 2ух времяни
     *
     * @param $time1 - время старта
     * @param $time2 - время конца
     * @return false|string - возвращает разницу времяни
     */
    public function dataTime($time1, $time2)
    {
        $time1 = new Datetime($time1);
        $time2 =  new Datetime($time2);

        $result = $time2->diff($time1);

        $time = strtotime($result->format('%H:%i:%s'));

        return date("H:i:s", $time);
    }


    /**
     *  Метод обрабатывает рабочие дни пользователей
     *
     * @param null $year - год
     * @param null $mouth - месяц
     * @return array
     */
    public function getWorksUsers($year = NULL, $mouth = NULL)
    {
        $result = [];

        if($year == NULL){
            $year = date('Y');
        }
        if($mouth == NULL){
            $mouth = date('m');
        }

        $auth = $this->getDI()->getSession()->get('auth');

        if($this->getDI()->getDispatcher()->getControllerName() == 'admin'){
            $user = users::query()
                ->columns(['name', 'id', 'login'])
                ->where('status = :status:')
                ->bind(['status' => 'Y'])
                ->execute();
        }
        else {
            $user = users::query()
                ->columns(['name', 'id', 'login'])
                ->where('status = :status: AND id <> :id:')
                ->bind(['status' => 'Y', 'id' => $auth['user_id']])
                ->execute();
        }

        $today = $this->getToday(FALSE);

        //Перебираем всех пользователей
        foreach($user->toArray() as $key => $value) {
            $result[$value['id']]['user_id'] = $value['id'];
            $result[$value['id']]['name'] = $value['name'];
            $result[$value['id']]['login'] = $value['login'];

            $work_user = parent::query()
                        ->columns(['id', 'day', 'start_time', 'stop_time', 'tardiness' , 'pause'])
                        ->where('day LIKE :day: AND user_id = :id:')
                        ->bind(['day' => $year.'-'.$mouth.'%' ,'id' => $value['id']])
                        ->execute();

            //Перебираем рабочее время на заданный год и месяц
            foreach ($work_user->toArray() as $k => $v){
                $num = $this->dayReplace($v['day']);
                $result[$value['id']]['work'][$num] = $work_user->toArray()[$k];


                //если сегодняшний день
                if($today == $v['day']){

                    //проверим сколько работник провел время на работе
                    if($v['stop_time'] == NULL) {
                        $time_day = $this->dataTime($v['start_time'], date('H:i:s'));
                    }
                    else{
                        $time_day = $this->dataTime($v['start_time'], $v['stop_time']);
                    }

                    //Вычтим паузу из рабочего дня
                    if($v['pause'] != null) {
                        $time_day = $this->AddTime($time_day, $v['pause'], FALSE);
                    }

                    $result[$value['id']]['work'][$num]['day_time'] = $time_day;

                }
                else {
                    //если не сегодняшний день и рабочий забыл нажать кнопку стоп то добавим указанное рабочее время
                    if($v['stop_time'] == NULL) {
                        $work_time = $this->getSettingWorkTime();

                        if($work_time) {
                            $work_time = $work_time->value;
                        }
                        else {
                            $work_time = "09:00:00";
                        }
                        $time_day = $this->AddTime($v['start_time'], $work_time);
                    }
                    else{
                        $time_day = $this->dataTime($v['start_time'], $v['stop_time']);
                    }

                    if($v['pause'] != null) {
                        $time_day = $this->AddTime($time_day, $v['pause'], FALSE);
                    }

                    $result[$value['id']]['work'][$num]['day_time'] = $time_day;
                }
            }
        }
        return $result;
    }


    /**
     *  Метод вычесляет рабочее время за месяц
     *
     * @param $count_day
     * @return mixed
     */
    public function getWorkTime($count_day)
    {
        $time = $this->getSettingWorkTime();

        if($time){
            $result = '09:00:00';
        }
        else{
            $result = $time->value;
        }

        $result = date('H', strtotime($result));

        return $count_day * $result;
    }


    /**
     *  Метод вычесляет рабочие дни и дни недели заданного месяца
     *
     * @param $month_day    - кол-во дней в месяца
     * @param $year         - год
     * @param $moth         - месяц
     * @return array
     */
    public function getWorkDay($month_day, $year, $moth)
    {
        //Высчитываем выходные субботу и воскресенье в месяце
        $work_day = 0;
        $all_work_day = [];
        for($i = 1; $month_day >= $i; $i++) {

            // Берем название недели и день
            $m = strftime('%A', strtotime($year . '-' . $moth . '-' . $i));

            switch ($m){
                case 'Monday'   : $m = 'Понедельник'; break;
                case 'Tuesday'  : $m = 'Вторник'; break;
                case 'Wednesday': $m = 'Среда'; break;
                case 'Thursday' : $m = 'Четверг'; break;
                case 'Friday'   : $m = 'Пятница'; break;
                case 'Saturday' : $m = 'Суббота'; break;
                case 'Sunday'   : $m = 'Воскресенье'; break;
            }

            $all_work_day[$i]['day'] = $m;
            $all_work_day[$i]['number'] = $i;

            //отсеиваем субботу и воскр
            $d = strftime('%u', strtotime($year . '-' . $moth . '-' . $i));
            if ($d == '6' || $d == '7') {
                continue;
            }
            $work_day++;
        }

        //Высчитываем праздничные дни заданные администрацией
        $weekend_day = weekend::find([
            "date LIKE :date: OR (date LIKE :mouth: AND rep= 'Y')",
            'bind'=>['date' => $year.'-'.$moth.'-%', 'mouth' => '%-'.$moth.'-%']
        ]);

        $weekend_day = $weekend_day->toArray();

        if(!empty($weekend_day)){
            $work_day = $work_day - count($weekend_day);
        }
        return ['work_day' => $work_day, 'all_work_day'=>$all_work_day];
    }


/**
 * @param null $date        - Year-month-day
 * @param null $id_user     - id пользователя
 */
    public function getDay($year = NULL, $mouth = NULL)
    {
        $result = [];

        if($year == NULL){
            $year = date('Y');
        }
        if($mouth == NULL){
            $mouth = date('m');
        }

        $result['year'] = $year;
        $result['mouth'] = $mouth;
        $result['today'] = $this->getToday();

        //Узнаем кол-во дней в месяце
        $all_day = cal_days_in_month(CAL_GREGORIAN, $mouth ,  $year);



        //Кол-во дней в месяце
        $day = $this->getWorkDay($all_day, $year, $mouth);

        //вычесляем кол-во рабочего времяни в месяц
        $result['work_time'] = $this->getWorkTime($day['work_day']);

        $result['work_day'] = $day['work_day'];
        $result['all_work_day'] = $day['all_work_day'];

        return $result;
    }


    /**
     *  Метод вычесляет рабоче дни за месяц авторизованного пользователя либо по ид
     *
     * @param null $year
     * @param null $mouth
     * @param null $user_id
     */
    public function getWorkAuth($year = NULL, $mouth = NULL, $user_id = NULL)
    {
        $result = [];

        if($year == NULL){
            $year = date('Y');
        }
        if($mouth == NULL){
            $mouth = date('m');
        }

        if($user_id == NULL){
            $user_id = $this->getDI()->getSession()->get('auth');

            $user_id = $user_id['user_id'];
        }

        $user = Users::query()
            ->columns(['name', 'login'])
            ->where('id = :id:')
            ->bind(['id' => $user_id])
            ->execute();

        $result = $user->toArray()['0'];

        $user_work = parent::query()
                        ->columns(['id', 'day', 'start_time', 'stop_time', 'pause', 'tardiness' ,'user_id'])
                        ->where('day LIKE :day: AND user_id = :id:')
                        ->bind(['day' => $year.'-'.$mouth.'-%', 'id' => $user_id])
                        ->execute();

        $auth_work = $user_work->toArray();

        //переменная для подсчета общего рабочго времяни за месяц
        $moth_time = NULL;
        $tardiness = 0;

        foreach($auth_work as $key => $value) {
            $num = $this->dayReplace($value['day']);
            $result['auth_work'][$num] = $auth_work[$key];

            //получим часы работы за день сотрудника
            $today = $this->getToday(FALSE);

            if($value['tardiness'] == 'Y'){
                $tardiness++;
            }


            if($today != $value['day']) {

                // если пользователь забыл нажать кнопку стоп
                if(empty($value['stop_time'])) {

                    $time = $this->getSettingWorkTime();

                    if ($time) {
                        $time = $time->value;
                    } else {
                        $time = '09:00:00';
                    }

                    $new_stop_time = $this->AddTime($value['start_time'], $time);

                    $result['auth_work'][$num]['stop_time'] = $new_stop_time;

                    //сохраним значение в БД
                    $save_time = parent::findFirst(['id = :id:','bind'=>['id' => $value['id']]]);
                    $save_time->stop_time = $new_stop_time;
                    $save_time->save();
                }

            }

            if($value['stop_time'] != NULL) {
                $time_day = $this->dataTime($value['start_time'], $value['stop_time']);
            }
            else{
                $time_day = $this->dataTime($value['start_time'], date("H:i:s"));
            }

            if($value['pause'] != null)
            {
                $time_day = $this->AddTime($time_day, $value['pause'], FALSE);
            }

            $result['auth_work'][$num]['all_time'] = $time_day;


            if($moth_time == "00:00:00"){
                $moth_time = $time_day;
            }
            else{
                $moth_time = $this->getMouthTimeWork($moth_time, $time_day);
            }

        }
        $result['mouth_time'] = $moth_time;
        $result['tardiness'] = $tardiness;

        return $result;
    }

    public function checkTardiness()
    {
        $time = settings::findFirst(['name = \'start-work-time\'']);


        if(strtotime($time->value) < strtotime(date('H:i'))){
            return true;
        }
        else{
            return false;
        }
    }

    /**
     *  Функция добавления в БД старт и стоп рабочего времяни
     *
     * @param $type
     * @return string
     */
    public function SaveTimeWork($type)
    {
        $result = ['status' => 'false'];

        $auth = $this->getDI()->getSession()->get('auth');

        $work_date = parent::findFirst([
            'day = :day: AND user_id = :id:',
            'bind' => ['day' => date('Y-m-d'), 'id' => $auth['user_id']]
        ]);

        //какая операция остановить время или стартануть
        if($type == 'save') {
            //Проверим был ли старт времяни до этого если нет то добавим новую строку в БД
            if($work_date != false) {
                //Если уже уже сущевствует старт в бд нужно посчитать паузу
                $work_date = $work_date->toArray();

                if($work_date['stop_time'] != '00:00:00') {

                    $time = $this->dataTime($work_date['stop_time'], date('H:i:s'));

                    if($work_date['pause'] != NUlL && $work_date['pause'] != '00:00:00') {
                        $time = $this->AddTime($time, $work_date['pause']);
                    }
                    $this->id = $work_date['id'];
                    $this->day = $work_date['day'];
                    $this->start_time = $work_date['start_time'];
                    $this->stop_time = NULL;
                    $this->pause = $time;
                    $this->user_id = $auth['user_id'];
                    $this->tardiness = $work_date['tardiness'];

                    if($this->save() == true) {
                        $result['pause'] = $time;
                        $result['status'] = 'true';
                    }
                }
            }
            else{

                if($this->checkTardiness() == true){
                    $this->tardiness = 'Y';
                }else{
                    $this->tardiness = NULL;
                }

                $this->day = date('Y-m-d');
                $this->start_time = date('H:i:s');
                $this->stop_time = NULL;
                $this->pause = NULL;
                $this->user_id = $auth['user_id'];



                if($this->save() == true) {
                    $result['start_time'] = $this->start_time;
                    $result['status'] = 'true';

                }
            }
        }
        else if($type == 'stop'){

            if($work_date != false) {

                $work_date = $work_date->toArray();

                $this->stop_time = date('H:i:s');
                $this->day = $work_date['day'];
                $this->id = $work_date['id'];
                $this->start_time = $work_date['start_time'];
                $this->pause = $work_date['pause'];
                $this->user_id = $work_date['user_id'];
                $this->tardiness = $work_date['tardiness'];

                if($this->save() == true) {
                    $result['stop_time'] = $this->stop_time;
                    $result['status'] = 'true';
                }
            }
        }

        return $result;
    }


    /**
     *  Вытаскивает из БД строчку работы по заданным данным
     * @param $id - ид работы
     * @param $param - день и юзер ид
     * @param $year - год работы
     * @param $mouth - месяц работы
     * @return Model
     */
    public function getWorkId($id, $param, $year, $mouth)
    {
        return $work = parent::findFirst([
            '(id = :id: AND user_id = :user:) OR (day = :day: AND user_id = :user:)',
            'bind'=>['id' => $id, 'user' => $param['user_id'], 'day' => $year.'-'.$mouth.'-'.$param['day']]
        ]);
    }

    /**
     * Метод сохраняет администратором в БД рабочее время пользователей
     *
     */
    public function setUsersWorkTime()
    {
        $result = ['status' => 'false'];

        $param = $this->getDI()->getRequest()->getPost('param');
        $year = $this->getDI()->getRequest()->getPost('year');
        $mouth = $this->getDI()->getRequest()->getPost('mouth');
        $val = $this->getDI()->getRequest()->getPost('val');
        $id = $this->getDI()->getRequest()->getPost('id');


        if($this->getDI()->getRequest()->getPost('type') == 'start'){


            $work = $this->getWorkId($id, $param, $year, $mouth);

            if(!$work){

                $this->day = $year.'-'.$mouth.'-'.$param['day'];
                $this->start_time = $val;
                $this->stop_time = NULL;
                $this->pause = NULL;
                $this->user_id = $param['user_id'];

                if($this->save())
                {
                    $result = ['status' => 'true'];
                }
            }
            else{
                $work->start_time = $this->getDI()->getRequest()->getPost('val');

                if($work->save())
                {
                    $result = ['status' => 'true'];
                }
            }

        }
        else if($this->getDI()->getRequest()->getPost('type') == 'stop'){


            $work = $this->getWorkId($id, $param, $year, $mouth);


            if(!$work){

                $this->day = $year.'-'.$mouth.'-'.$param['day'];
                $this->start_time = '00:00:00';
                $this->stop_time = $val;
                $this->pause = NULL;
                $this->user_id = $param['user_id'];

                if($this->save())
                {
                    $result = ['status' => 'true'];
                }
            }
            else{
                $work->stop_time = $this->getDI()->getRequest()->getPost('val');

                if($work->save())
                {
                    $result = ['status' => 'true'];
                }
            }

        }
        else if($this->getDI()->getRequest()->getPost('type') == 'pause'){

            $work = $this->getWorkId($id, $param, $year, $mouth);

            if(!$work){

                $this->day = $year.'-'.$mouth.'-'.$param['day'];
                $this->start_time = '00:00:00';
                $this->stop_time = '00:00:00';
                $this->pause = $val;
                $this->user_id = $param['user_id'];

                if($this->save())
                {
                    $result = ['status' => 'true'];
                }
            }
            else{
                $work->pause = $this->getDI()->getRequest()->getPost('val');

                if($work->save())
                {
                    $result = ['status' => 'true'];
                }
            }
        }
        else if($this->getDI()->getRequest()->getPost('type') == 'tardiness'){

            $work = $this->getWorkId($id, $param, $year, $mouth);

            if($work){
                if($this->getDI()->getRequest()->getPost('val') == ''){
                    $param = NULL;
                }
                else{
                    $param = $this->getDI()->getRequest()->getPost('val');
                }

                $work->tardiness = $param;

                if($work->save()){
                    $result = ['status' => 'true'];
                }
            }
        }
        return $result;
    }
}
