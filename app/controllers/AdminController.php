<?php

use Phalcon\Mvc\Controller;

class AdminController extends Controller
{
    public function initialize()
    {
        $name_project = settings::findFirst(['name = \'project-name\'']);
        $this->tag->prependTitle($name_project->value.' | ');
        $this->view->setTemplateAfter('AdminLayout');
    }

    public function IndexAction()
    {
        $this->tag->setTitle('Рабочее время пользователей');

        $form = new WorkListForm();
        $works = new works();

        if($this->request->isPost()) {

            // Если пост запрос
            if($form->isValid($this->request->getPost())) {
                //вызываем список времяни работы сотруников за месяц
                $users = $works->getWorksUsers($this->request->getPost('year'), $this->request->getPost('mouth'));

                $day = $works->getDay($this->request->getPost('year'), $this->request->getPost('mouth'));
            }
            else{
                foreach ($form->getMessages() as $message) {
                    $this->flash->error($message);
                }
            }
        }
        else if(!$this->request->isPost()) {
            //если гет
            $works = new works();

            $users = $works->getWorksUsers();

            $day = $works->getDay();
        }

        $this->view->setVar('users', $users);

        //print_die($day);
        $this->view->setVar('params', $day);

        $this->view->form = $form;
    }

    public function SavetWorkTimeAction()
    {

        if($this->request->isPost() && $this->request->isAjax()){

            $works = new works();

            echo json_encode($works->setUsersWorkTime());

        }
        else {
            return $this->dispatcher->forward([
                'controller' => 'error',
                'action'     => 'show404'
            ]);
        }
        $this->view->disable();
        return true;
    }

    //Список пользователей
    public function ListUserAction()
    {
        $this->tag->setTitle('Список пользователей');

        $form = new AdminUserForm();

        if($this->request->isPost())
        {
            $params = $this->request->getPost();

            foreach ($params['user'] as $val => $key)
            {
                //занесем хэш токен формы для проверки валидации формы
                $params['user'][$val]['hash'] = $this->request->getPost('hash');

                if($form->isValid($params['user'][$val])) {

                    //Сохраняем пользователя
                    $user = new users();

                    $result = $user->SaveUser($params['user'][$val]);
                }
                else{
                    foreach ($form->getMessages() as $message){
                        $this->flash->error($message);
                    }
                    break;
                }
            }

            if(isset($result) && $result == true )
                $this->flash->success('Данные успешно сохранились');
            else
                $this->flash->warning('произошла ошибка повторить попытку заново');
        }

        $users = Users::find();
        $this->view->setVar('users', $users->toArray());

        $this->view->form = $form;
    }

    //Добавление нового пользователя
    public function AddUserAction()
    {
        $this->tag->setTitle('Добавить нового пользователя');

        $form = new AdminUserForm();

        if($this->request->isPost()) {
            if($form->isValid($this->request->getPost()) != false )
            {
                $user = new Users();

                $result = $user->SaveUser($this->request->getPost());

                if($result)
                    $this->flash->success('Пользователь был успешно создан');
                else
                    $this->flash->warning('произошла ошибка повторить попытку заново');

                $form->clear();
            }
            else{
                $this->flash->error($form->getMessages());
            }
        }

        $this->view->form = $form;
    }

    public function weekendAction()
    {
        $this->tag->setTitle('Выходные дни');

        $form = new WorkListForm();

        $weekend= new weekend();

        $weekends = $weekend->getWeekends();

        if ($this->request->isPost()) {

            $day = $weekend->getCaledar($this->request->getPost('year'), $this->request->getPost('mouth'));
        }
        else if (!$this->request->isPost()) {

            $day = $weekend->getCaledar();
        }

        $this->view->setVar('params', $day);

        $this->view->setVar('weekends', $weekends);
        //print_die($weekend->toArray());

        $this->view->form = $form;
    }

    public function SetWeekendAction()
    {
        if($this->request->isPost() && $this->request->isAjax()){

            $weekend = new weekend();

            echo json_encode($weekend->setWeekend());

        }
        else {
            return $this->dispatcher->forward([
                'controller' => 'error',
                'action'     => 'show404'
            ]);
        }
        $this->view->disable();
        return true;
    }

    public function weekenddeleteAction()
    {

        if(!$this->request->isPost()){

            $weekend = new weekend();


            if($weekend->deleteWeekend($this->dispatcher->getParams('0'))){
                $this->flash->success('Прадзник бы удален');
            }else{
                $this->flash->error('Произошла ошибка');
            }
            return $this->response->redirect('admin/weekend');
        }
        else {
            return $this->dispatcher->forward([
                'controller' => 'error',
                'action'     => 'error404'
            ]);
        }
    }

    public function settingsAction()
    {
        $this->tag->setTitle('Настройки проекта');

        $form = new settingsForms();

        $settings = new settings();

        if($this->request->isPost())
        {

            if($form->isValid($this->request->getPost()))
            {
                if(strtotime($this->request->getPost('work-time')) && strtotime($this->request->getPost('start-work-time'))){
                    $settings->setSettings();
                }
                else{
                    $this->flash->error('Не корректно задано время');
                }
            }
            else{
                foreach ($form->getMessages() as $message) {
                    $this->flash->error($message);
                }
            }
        }

            $param = $settings->getSettings();
//print_die($param);
            $this->view->setVar('param', $param);

        $this->view->form = $form;
    }
}