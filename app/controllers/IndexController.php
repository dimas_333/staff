<?php

use Phalcon\Mvc\Controller;

class IndexController extends Controller
{

    public function initialize()
    {
        $name_project = settings::findFirst(['name = \'project-name\'']);
        $this->tag->prependTitle($name_project->value.' | ');
        $user = $this->session->get('auth');
        if($user['Type'] == 'A'){
            $this->view->setTemplateAfter('AdminLayout');
        }
        else{
            $this->view->setTemplateAfter('UserLayout');
        }
    }

    public function IndexAction()
    {
        $this->tag->setTitle('Главная страница');

        $form = new WorkListForm();
        $works = new works();

        if($this->request->isPost()) {
            //если пост
            if($form->isValid($this->request->getPost())) {

                $users = $works->getWorksUsers($this->request->getPost('year'), $this->request->getPost('mouth'));

                $day = $works->getDay($this->request->getPost('year'), $this->request->getPost('mouth'));

                $auth_work = $works->getWorkAuth($this->request->getPost('year'), $this->request->getPost('mouth'));
            }
            else{
                foreach ($form->getMessages() as $message) {
                    $this->flash->error($message);
                }
            }
        }
        else if(!$this->request->isPost()) {
            //если гет

            $day = $works->getDay();

            $users = $works->getWorksUsers();

            $auth_work = $works->getWorkAuth();
        }

        $this->view->setVar('users', $users);

        $this->view->setVar('auth', $auth_work);

       // print_die($auth_work);

        $this->view->setVar('params', $day);

        $this->view->form = $form;
    }

    public function SaveTimeAction()
    {
        if($this->request->isPost())
        {
            if($this->request->isAjax() && !empty($this->request->getPost('type' , 'string'))){

                $working = new works();

                $work = $working->SaveTimeWork($this->request->getPost('type' , 'string'));

                echo json_encode($work);
            }
        }
        else
        {
            return $this->dispatcher->forward([
                'controller' => 'error',
                'action'     => 'error404'
            ]);
        }

        $this->view->disable();
        return true;
    }


    public function changeAction()
    {
        if($this->request->isPost())
        {
            if($this->request->isAjax()){

                $user = new users();

                if($user->changePassword()){
                    echo json_encode(['status' => 'true', 'message' => 'Пароль был изменен']);
                }else{
                    echo json_encode(['status'=>'false', 'message' => 'Не верно введен логин или пароль']);
                }
            }
        }
        else
        {
            return $this->dispatcher->forward([
                'controller' => 'error',
                'action'     => 'error404'
            ]);
        }

        $this->view->disable();
        return true;
    }
}
