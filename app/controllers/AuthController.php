<?php

use Phalcon\Mvc\Controller;

class AuthController extends Controller
{
    public function initialize()
    {

    }

    public function userAuthAction()
    {
        if($this->session->get('auth') != NULL) {
            return $this->dispatcher->forward([
                'controller' => 'error',
                'action'     => 'error404'
            ]);
        }

        $form = new AuthIndexForm();

        //Если POST запрос
        if($this->request->isPost()) {

            if($form->isValid($this->request->getPost()) != false) {


                $user = new users();

                //print_die($user->CheckUser('U'));

                //Проверим сущевствует-ли указанный пользователь
                if($user->CheckUser('U') == true)
                {
                    return $this->response->redirect('/home');
                }
            }
            else {

                foreach($form->getMessages() as $message)
                {
                    $this->flash->error($message);
                }
            }
        }

        $this->view->form = $form;


    }
    public function adminAuthAction()
    {
        if($this->session->get('auth') !== NULL) {
            return $this->dispatcher->forward([
                'controller' => 'error',
                'action'     => 'error404'
            ]);
        }

        $form = new AuthIndexForm();


        //Если POST запрос
        if($this->request->isPost()) {

            if($form->isValid($this->request->getPost()) != false) {

                $user = new users();

                //Проверим сущевствует-ли указанный пользователь
                if($user->CheckUser('A') == true)
                {
                    return $this->response->redirect('admin/index');
                }
            }
            else {

                foreach($form->getMessages() as $message)
                {
                    $this->flash->error($message);
                }
            }
        }

        $this->view->form = $form;

    }

    public function LogoutAction()
    {
        $auth = $this->session->get('auth');

        if($auth == NULL) {
            return $this->dispatcher->forward([
                'controller' => 'error',
                'action'     => 'error404'
            ]);
        }

        $this->session->destroy();

        return $this->response->redirect('/login');
    }
}