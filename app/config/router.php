<?php

$router = $di->getRouter();

// Define your routes here

$router->add('/admin', [
    'controller' => 'auth',
    'action'     => 'adminauth'
]);
$router->add('/login', [
    'controller' => 'auth',
    'action'     => 'userauth'
]);
$router->add('/home', [
    'controller' => 'index',
    'action'     => 'index'
]);
$router->add('/exit', [
    'controller' => 'auth',
    'action'     => 'logout'
]);

$router->handle();
