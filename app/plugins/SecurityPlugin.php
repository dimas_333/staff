<?php

use Phalcon\Acl;
use Phalcon\Acl\Role;
use Phalcon\Acl\Resource;
use Phalcon\Events\Event;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Acl\Adapter\Memory as AclList;


class SecurityPlugin extends Plugin
{

    public function getAcl()
    {
        //unset($this->persistent->acl);
        if(!isset($this->persistent->acl)) {
            $acl = new AclList();

            $acl->setDefaultAction(Acl::DENY);


            // Регистрируем роли
            $roles = [
                'admin' => new Role(
                  'Admin',
                  'Привилегии для администратора'
                ),
                'user'  => new Role(
                    'User',
                    'Привилегии для пользователей'
                ),
                'guest' => new Role(
                    'Guest',
                    'Привилегии для гостей'
                )
            ];

            foreach ($roles as $role) {
                $acl->addRole($role);
            }

            //Ресурсы для администратора
            $adminResource = [
                'admin'    => ['index', 'savetworktime', 'listuser', 'adduser', 'weekend', 'setweekend', 'weekenddelete', 'settings'],
                'index'    => ['index', 'savetime', 'change']
            ];

            foreach ($adminResource as $resource => $actions) {
                $acl->addResource(new Resource($resource), $actions);
            }

            //Ресурсы для пользователей
            $userResource = [
                'index'      => ['index', 'savetime', 'change' ]
            ];

            foreach ($userResource as $resource => $actions) {
                $acl->addResource(new Resource($resource), $actions);
            }

            $guestResource = [
                'auth'      => ['userauth', 'adminauth', 'logout' ],
                'error'     => ['error404', 'error401']
            ];

            foreach ($guestResource as $resource => $actions) {
                $acl->addResource(new Resource($resource), $actions);
            }

            //Предоставление доступа к общественным местам как пользователям, так и гостям
            foreach ($roles as $role) {
                foreach ($guestResource as $resource => $actions) {
                    foreach ($actions as $action){
                        $acl->allow($role->getName(), $resource, $action);
                    }
                }
            }

            //Предоставить доступ к зоне для роли Пользователи
            foreach ($userResource as $resource => $actions) {
                foreach ($actions as $action){
                    $acl->allow('User', $resource, $action);
                }
            }

            //Предоставить доступ к зоне для роли Администратор
            foreach ($adminResource as $resource => $actions) {
                foreach ($actions as $action){
                    $acl->allow('Admin', $resource, $action);
                }
            }

            //Acl хранится в сеансе, APC тоже будет полезен
            $this->persistent->acl = $acl;
        }

        return $this->persistent->acl;
    }

    public function beforeExecuteRoute(Event $event, Dispatcher $dispatcher)
    {
        $auth = $this->session->get('auth');

        if ($auth !== NULL){
            switch($auth['Type'])
            {
                case 'A': $role = 'Admin'; break;
                case 'U': $role = 'User'; break;
            }
        } else {
            $role = 'Guest';
        }

        $controller = $dispatcher->getControllerName();
        $action = $dispatcher->getActionName();


        if($controller == 'index' && $role == 'Guest') {
            $this->getDI()->getResponse()->redirect('/login');
        }

        if($controller == 'admin' && $role == 'Guest'){
            $this->getDI()->getResponse()->redirect('/admin');
        }

        $acl = $this->getAcl();


        if (!$acl->isResource($controller)) {
            $dispatcher->forward([
                'controller' => 'error',
                'action'     => 'error404'
            ]);

            return false;
        }


        $allowed = $acl->isAllowed($role, $controller, $action);
        if (!$allowed) {
            $dispatcher->forward([
                'controller' => 'error',
                'action'     => 'error401'
            ]);
            return false;
        }
    }
}