<?php
use Phalcon\Forms\Form;

use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Date;
use Phalcon\Forms\Element\Submit;


use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Date as ValidDate;

class settingsForms extends Form
{
    public function initialize()
    {
        $project_name = new Text('project_name');
        $project_name->setLabel('Имя проекта');
        $project_name->addValidators([
            new PresenceOf([
                'message'=> 'Введите назавние проекта'
            ])
        ]);

        $this->add($project_name);



        $submit = new Submit('submit');

        $this->add($submit);
    }
}