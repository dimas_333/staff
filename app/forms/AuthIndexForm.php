<?php
use Phalcon\Forms\Form;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Identical;

use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Submit;



class AuthIndexForm extends Form
{
    public function initialize()
    {
        //Login
        $login = new Text('login');
        $login->addValidators([
           new PresenceOf([
               'message' => 'Введите логин'
           ])
        ]);
        $login->clear();

        $this->add($login);

        //password
        $password = new Password('password');

        $password->addValidators([
            new PresenceOf([
                'message' => 'Введите пароль'
            ])
        ]);

        $password->clear();
        $this->add($password);


        //Token
        $token = new Hidden('hash');
        $token->addValidator(new Identical([
            'value' => $this->security->getSessionToken(),
            'message' => 'CSRF Валидация пуста повторите попытку еще раз'
        ]));

        $token->clear();
        $this->add($token);

        $submit = new Submit('submit');
        $this->add($submit);
    }
}

