<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Submit;

use Phalcon\Validation\Validator\Numericality;
use Phalcon\Mvc\Model\Validator\PresenceOf;

class WorkListForm extends Form
{
    public function initialize()
    {

        $year = new Select('year');

        $year->addOption([
                    '2015' => '2015',
                    '2016' => '2016',
                    '2017' => '2017',
                    '2018' => '2018'
        ]);

        $year->addValidators([
            new Numericality([
                    'message' => 'Год указан не верно'
            ])
        ]);

        $this->add($year);

        $mouth = new Select('mouth');
        $mouth->addOption([
            '01' => 'Январь',
            '02' => 'Февраль',
            '03' => 'Март',
            '04' => 'Апрель',
            '05' => 'Май',
            '06' => 'Июнь',
            '07' => 'Июль',
            '08' => 'Август',
            '09' => 'Сентябрь',
            '10' => 'Октябрь',
            '11' => 'Ноябрь',
            '12' => 'Декабрь',
        ]);
        $mouth->addValidators([
            new Numericality([
                'message' => 'Месяц указан не верно'
            ])
        ]);

        $this->add($mouth);

        $submit = new Submit('submit');

        $this->add($submit);
    }
}