<?php
use Phalcon\Forms\Form;

use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Email;
use Phalcon\Forms\Element\Select;

use Phalcon\Validation\Validator\Email as ValidMail;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Numericality;

class AdminUserForm extends Form
{

    public function initialize()
    {

        //user id
        $user_id = new Hidden('user-id');

        $user_id->addValidators([
            new Numericality([
                    "message" => "Ошибка id user",
                ])
        ]);
        $this->add($user_id);

        //Login
        $login = new Text('login');
        $login->setLabel('Логин');
        $login->addValidators([
            new PresenceOf([
                'message' => 'Введите логин пользователя'
            ])
        ]);

        $this->add($login);

        //name
        $name = new Text('name');
        $name->setLabel('Имя пользователя');
        $name->addValidators([
            new PresenceOf([
                'message' => 'Введите имя пользователя :'
            ])
        ]);

        $this->add($name);

        //password
        $password = new Password('password');
        $password->setLabel('Пароль пользователя');
        $password->addValidators([
            new PresenceOf([
                'message' => 'Введите пароль для пользователя'
            ])
        ]);

        $this->add($password);

        //email
        $email = new Email('email');
        $email->setLabel('Почта пользователя');
        $email->addValidators([
            new PresenceOf([
              'message' => 'Введите почту пользователя'
            ]),
            new ValidMail([
                'message' => 'Не верный формат почты'
            ])
        ]);
        $this->add($email);

        //type User
        $type = new Select('type');
        $type->setLabel('Тип пользователя');
        $type->addOption(['A' => 'Администратор', 'U' => 'Пользователь']);
        $type->addValidators([
           new PresenceOf([
               'message' => 'Ошибка не верно введено занчение типа'
           ])
        ]);

        $this->add($type);

        $status = new Select('status');
        $status->setLabel('Статус пользователя');
        $status->addOption(['Y' => 'Активен', 'N' => 'Не активен']);
        $status->addValidators([
            new PresenceOf([
                'message' => 'Ошибка не верно введено занчение статуса'
            ])
        ]);

        $this->add($status);

        //print_arr($this->security->getSessionToken());
        //Token
        $token = new Hidden('hash');
        $token->addValidator(new Identical([
            'value' => $this->security->getSessionToken(),
            'message' => 'CSRF Валидация пуста повторите попытку еще раз'
        ]));

        $this->add($token);

        $submit = new Submit('submit');
        $this->add($submit);
    }
}