<div class="head-info">
    <p><strong>Кол-во часов в месяц:</strong> <?= $params['work_time'] ?> ч</p>
    <?= $this->tag->form(['admin/index']) ?>
        <div class="row">
                <div class="col-6">
                    <?= $form->render('year', ['class' => 'form-control year', 'value' => $params['year']]) ?>
                </div>
                <div class="col-6">
                    <?= $form->render('mouth', ['class' => 'form-control mouth', 'value' => $params['mouth']]) ?>
                </div>
        </div>
        <?= $form->render('submit', ['class' => 'd-none set-work']) ?>
    </form>
</div>
<div class="filter-user">
    <div class="container-fluid">
        <div class="row">
            <div class="col-1 text-right">
                <strong class="align-middle">Фильтр</strong>
            </div>
            <div class="col-11">
                <select class='form-control user-filter'>
                    <option name="user-value" value="all">Все</option>
                    <?php foreach ($users as $user) { ?>
                        <option name="user-value" value="<?= $user['login'] ?>"><?= $user['name'] ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
</div>
<div class="content-info">
    <table class="table table-bordered">
        <thead>
                <th class="info-table"><a href="#" class="hide-table">Скрыть/раскрыть</a></th>
            <?php foreach ($users as $user) { ?>
                <th class="users <?= $user['login'] ?> "><?= $user['name'] ?></th>
            <?php } ?>
        </thead>
        <tbody>
        <?php foreach ($params['all_work_day'] as $param) { ?>
            <tr <?= $this->elements->setClassTodayClass($param['number'], 'class', 'today') ?> class="hide">
                <td class="text-center align-middle" >
                    <?= $param['number'] ?>
                    <br />
                    <?= $param['day'] ?>
                </td>
                <?php foreach ($users as $user) { ?>
                    <td class="text-center align-middle info-table users <?= $user['login'] ?>">
                        <?php if (isset($user['work'][$param['number']])) { ?>

                            <div class="info-work">
                                <div class="work-time">
                                    <div class="form-group row">
                                        <label for="example-time-input" class="col-3 col-form-label">Старт</label>
                                        <div class="col-9">
                                            <input class="form-control save-start-time"
                                                   name="<?= $user['work'][$param['number']]['id'] ?>"
                                                   id="example-time-input"
                                                   type="time"
                                                   data-item='{"day" : "<?= $param['number'] ?>", "user_id" : "<?= $user['user_id'] ?>"}'
                                                   value="<?= $user['work'][$param['number']]['start_time'] ?>"
                                            >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <br />
                                        <label for="example-time-input" class="col-3 col-form-label">Стоп</label>
                                        <div class="col-9">
                                            <input class="form-control save-stop-time"
                                                   name="<?= $user['work'][$param['number']]['id'] ?>"
                                                   id="example-time-input"
                                                   type="time"
                                                   data-item='{"day" : "<?= $param['number'] ?>", "user_id" : "<?= $user['user_id'] ?>" }'
                                                   value="<?= $user['work'][$param['number']]['stop_time'] ?>"
                                            >
                                        </div>
                                    </div>
                                    <div class="pause-time">
                                        <div class="form-group row">
                                            <label for="example-time-input" class="col-3 col-form-label">Пауза</label>
                                            <div class="col-9">
                                                <input class="form-control save-pause-time"
                                                       name="<?= $user['work'][$param['number']]['id'] ?>"
                                                       id="example-time-input"
                                                       type="time"
                                                       data-item='{"day" : "<?= $param['number'] ?>", "user_id" : "<?= $user['user_id'] ?>" }'
                                                       value="<?= $user['work'][$param['number']]['pause'] ?>"
                                                >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tardiness">
                                        <div class="form-group row">
                                            <label class="example-time-input col-4 col-form-label" for="defaultCheck1">Опаздание</label>
                                            <div class="col-8 text-left align-middle">
                                                <input class="form-check-label save-tardiness-time"
                                                       name="<?= $user['work'][$param['number']]['tardiness'] ?>"
                                                       id="example-time-input"
                                                       type="checkbox"
                                                       data-item='{"day" : "<?= $param['number'] ?>", "user_id" : "<?= $user['user_id'] ?>" }'
                                                       <?php if ($user['work'][$param['number']]['tardiness'] == 'Y') { ?>
                                                       checked="checked"
                                                       <?php } ?>
                                                >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="day-work-time">
                                        Рабочее время: <?= $user['work'][$param['number']]['day_time'] ?>
                                    </div>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="work-time">
                                <div class="form-group row">
                                    <label for="example-time-input" class="col-3 col-form-label">Старт</label>
                                    <div class="col-9">
                                        <input class="form-control save-start-time"
                                               id="example-time-input"
                                               type="time"
                                               data-item='{"day" : "<?= $param['number'] ?>", "user_id" : "<?= $user['user_id'] ?>" }'
                                               value="00:00"
                                        >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <br />
                                    <label for="example-time-input" class="col-3 col-form-label">Стоп</label>
                                    <div class="col-9">
                                        <input class="form-control save-stop-time"
                                               id="example-time-input"
                                               type="time"
                                               data-item='{"day" : "<?= $param['number'] ?>", "user_id" : "<?= $user['user_id'] ?>" }'
                                               value="00:00"
                                        >
                                    </div>
                                </div>
                                <div class="pause-time">
                                    <div class="form-group row">
                                        <label for="example-time-input" class="col-3 col-form-label">Пауза</label>
                                        <div class="col-9">
                                            <input class="form-control save-pause-time"
                                                   id="example-time-input"
                                                   type="time"
                                                   data-item='{"day" : "<?= $param['number'] ?>", "user_id" : "<?= $user['user_id'] ?>" }'
                                                   value="00:00"
                                            >
                                        </div>
                                    </div>
                                </div>
                                <div class="day-work-time">
                                </div>
                            </div>
                        <?php } ?>
                    </td>
                <?php } ?>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<?= $this->tag->javascriptInclude('/public/js/works.js') ?>
<?= $this->tag->javascriptInclude('/public/js/admin/saveWorks.js') ?>