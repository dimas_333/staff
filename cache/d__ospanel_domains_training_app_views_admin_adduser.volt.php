<?= $this->tag->form(['admin/adduser']) ?>
    <?= $form->render('hash', ['value' => $this->security->getToken()]) ?>
    <?php if (isset($user)) { ?>
        <?php $id = $user['user-id']; ?>
    <?php } else { ?>
        <?php $id = 0; ?>
    <?php } ?>


<?= $form->render('user-id', ['value' => $id]) ?>
    <div class="container-fluid">
        <div class="col-12 text-center">
            <h3>Добавление нового работника</h3>
            <br />
        </div>
        <div class="col-12">
            <div class="form-group row">
                <?= $form->label('login', ['for' => 'example-text-input', 'class' => 'col-6 col-form-label text-right']) ?>
                <div class="col-6">
                    <?= $form->render('login', ['class' => 'form-control']) ?>
                </div>
            </div>
            <div class="form-group row">
                <?= $form->label('name', ['for' => 'example-text-input', 'class' => 'col-6 col-form-label text-right']) ?>
                <div class="col-6">
                    <?= $form->render('name', ['class' => 'form-control']) ?>
                </div>
            </div>
            <div class="form-group row">
                <?= $form->label('password', ['for' => 'example-text-input', 'class' => 'col-6 col-form-label text-right']) ?>
                <div class="col-6">
                    <?= $form->render('password', ['class' => 'form-control']) ?>
                </div>
            </div>
            <div class="form-group row">
                <?= $form->label('email', ['for' => 'example-text-input', 'class' => 'col-6 col-form-label text-right']) ?>
                <div class="col-6">
                    <?= $form->render('email', ['class' => 'form-control']) ?>
                </div>
            </div>
            <div class="form-group row">
                <?= $form->label('type', ['for' => 'example-text-input', 'class' => 'col-6 col-form-label text-right']) ?>
                <div class="col-6">
                    <?= $form->render('type', ['class' => 'form-control']) ?>
                </div>
            </div>
            <div class="form-group row">
                <?= $form->label('status', ['for' => 'example-text-input', 'class' => 'col-6 col-form-label text-right']) ?>
                <div class="col-6">
                    <?= $form->render('status', ['class' => 'form-control']) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-12 text-center">
                    <?= $form->render('submit', ['class' => 'btn btn-primary', 'value' => 'Добавить']) ?>
                </div>
            </div>
        </div>
    </div>
</form>