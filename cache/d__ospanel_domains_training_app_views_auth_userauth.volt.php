<?= $this->tag->stylesheetLink('/public/css/auth/user.css') ?>

<?= $this->flash->output() ?>
<?= $this->tag->form(['/login', 'id' => 'slick-login']) ?>
    <?= $form->render('hash', ['value' => $this->security->getSessionToken()]) ?>
    <?= $form->render('login', ['name' => 'login', 'class' => 'placeholder', 'placeholder' => 'Логин']) ?>
    <?= $form->render('password', ['name' => 'password', 'class' => 'placeholder', 'placeholder' => 'Пароль']) ?>
    <?= $form->render('submit', ['value' => 'Войти']) ?>
</form>