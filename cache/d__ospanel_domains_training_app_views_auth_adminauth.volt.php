<?= $this->tag->stylesheetLink('/public/css/auth/admin.css') ?>

<?= $this->flash->output() ?>
<section class="container">
    <div class="login">
        <h1>Войти в личный кабинет</h1>
        <?= $this->tag->form(['/admin']) ?>
        <?= $form->render('hash', ['value' => $this->security->getSessionToken()]) ?>
        <p>
            <?= $form->render('login', ['name' => 'login', 'placeholder' => 'Логин или Email']) ?>
        </p>
        <p>
            <?= $form->render('password', ['name' => 'password', 'placeholder' => 'Пароль']) ?>
        </p>
        <p class="remember_me">
        </p>
        <p class="submit">
            <?= $form->render('submit', ['value' => 'Войти']) ?>
        </p>
        </form>
    </div>
</section>
