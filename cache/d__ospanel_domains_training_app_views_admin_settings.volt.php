<div class="container-fluid" style="margin-top: 10px">
    <div class="row">
        <div class="col-12 text-center">
            <h3>Настройки</h3>
            <br />
        </div>
    </div>
    <div>
        <div class="container-fluid">
            <div class="row justify-content-md-center">
                <div class="col-md-auto">
                    <?= $this->tag->form(['/admin/settings']) ?>
                        <div class="form-group row">
                            <?= $form->label('project_name', ['for' => 'example-text-input', 'class' => 'col-6 col-form-label text-right']) ?>
                            <div class="col-6">
                                <?= $form->render('project_name', ['class' => 'form-control', 'value' => $param['project-name']['value']]) ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-6 col-form-label text-right">Рабочее время</label>
                            <div class="col-6">
                                <input type="time" name="work-time" class="form-control" value="<?= $param['work-time']['value'] ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-6 col-form-label text-right">Начало рабочего дня</label>
                            <div class="col-6">
                                <input type="time" name="start-work-time" class="form-control" value="<?= $param['start-work-time']['value'] ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12 text-center">
                                <?= $form->render('submit', ['value' => 'Схоранить', 'class' => 'btn btn-primary']) ?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>