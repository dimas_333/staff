<?= $this->tag->form(['admin/listuser', ['method' => 'post']]) ?>
<div class="container-fluid" style="margin-top: 10px">
    <div class="row">
        <div class="col-6">
            <h3>Список всех пользователей</h3>
            <br />
        </div>
        <div class="col-6 text-right">
            <?= $form->render('submit', ['value' => 'Схоранить', 'class' => 'btn btn-primary']) ?>
        </div>
        <div class="row table">
            <div class="col-12">
                <table>
                    <thead>
                    <tr>
                        <th>  <?= $form->label('login') ?> </th>
                        <th>  <?= $form->label('name') ?> </th>
                        <th>  <?= $form->label('password') ?> </th>
                        <th>  <?= $form->label('email') ?> </th>
                        <th>  <?= $form->label('type') ?> </th>
                        <th>  <?= $form->label('status') ?> </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?= $form->render('hash', ['value' => $this->security->getSessionToken()]) ?>
                    <?php foreach ($users as $user) { ?>
                        <?php $id = $user['id']; ?>
                        <tr>
                            <?= $form->render('user-id', ['name' => 'user[' . $user['id'] . '][user-id]', 'value' => $user['id']]) ?>
                            <td>
                                <?= $form->render('login', ['name' => 'user[' . $user['id'] . '][login]', 'value' => $user['login'], 'class' => 'form-control']) ?>
                            </td>
                            <td>
                                <?= $form->render('name', ['name' => 'user[' . $user['id'] . '][name]', 'value' => $user['name'], 'class' => 'form-control']) ?>
                            </td>
                            <td>
                                <?= $form->render('password', ['name' => 'user[' . $user['id'] . '][password]', 'value' => $user['password'], 'class' => 'form-control']) ?>
                            </td>
                            <td>
                                <?= $form->render('email', ['name' => 'user[' . $user['id'] . '][email]', 'value' => $user['email'], 'class' => 'form-control']) ?>
                            </td>
                            <td>
                                <?= $form->render('type', ['name' => 'user[' . $user['id'] . '][type]', 'value' => $user['type'], 'class' => 'form-control']) ?>
                            </td>
                            <td>
                                <?= $form->render('status', ['name' => 'user[' . $user['id'] . '][status]', 'value' => $user['status'], 'class' => 'form-control']) ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</form>