<!doctype html>
<html lang="ru">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= $this->url->get('/public/css/bootstrap.min.css') ?>" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <?= $this->tag->javascriptInclude('/public/js/jquery-3.3.1.js') ?>
    <?= $this->tag->stylesheetLink('/public/css/font-awesome.css') ?>
    <link rel="shortcut icon" type="image/x-icon" href="<?= $this->url->get('/public/img/favicon.ico') ?>"/>
    <?= $this->tag->gettitle() ?>
</head>
<body>

    <?= $this->getContent() ?>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <?= $this->tag->javascriptInclude('/public/js/popper.min.js', ['integrity' => 'sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49', 'crossorigin' => 'anonymous']) ?>
    <?= $this->tag->javascriptInclude('/public/js/bootstrap.min.js', ['integrity' => 'sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy', 'crossorigin' => 'anonymous']) ?>

</body>
</html>