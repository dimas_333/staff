<?php foreach ($users as $user) { ?>
    <td class="text-center">
        <?php if (isset($user['work'][$param['number']])) { ?>
            <div class="work-time">
                <?= $user['work'][$param['number']]['start_time'] ?>
                -
                <?= $user['work'][$param['number']]['stop_time'] ?>
            </div>
            <div class="pause-time">
                <?php if (isset($user['work'][$param['number']]['pause'])) { ?>
                       Пауза: <?= $user['work'][$param['number']]['pause'] ?>
                <?php } ?>
            </div>
            <div class="day-work-time">
                <?php if (isset($user['work'][$param['number']]['day_time'])) { ?>
                    Отработано : <?= $user['work'][$param['number']]['day_time'] ?>
                <?php } ?>
            </div>
        <?php } ?>
    </td>
<?php } ?>