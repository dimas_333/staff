<?php if (isset($auth['auth_work'][$param['number']])) { ?>
    <div class="work-time">
        <label <?= $this->elements->setClassTodayClass($param['number'], 'id', 'start_time') ?> class="start_time">
                <?= $auth['auth_work'][$param['number']]['start_time'] ?>
        </label>
        -
        <label <?= $this->elements->setClassTodayClass($param['number'], 'id', 'stop_time') ?> class="stop_time">
            <?= $auth['auth_work'][$param['number']]['stop_time'] ?>
        </label>
    </div>
    <div <?= $this->elements->setClassTodayClass($param['number'], 'id', 'pause-time') ?> class="pause-time">
       <?php if (isset($auth['auth_work'][$param['number']]['pause'])) { ?>
             Пауза:  <?= $auth['auth_work'][$param['number']]['pause'] ?>
       <?php } ?>
    </div>
    <div class="day-work-time">
        <?php if (isset($auth['auth_work'][$param['number']]['all_time'])) { ?>
           Отработано:  <?= $auth['auth_work'][$param['number']]['all_time'] ?>
        <?php } ?>
    </div>
<?php } else { ?>
    <div class="work-time">

        <label <?= $this->elements->setClassTodayClass($param['number'], 'id', 'start_time') ?> class="start_time">

        </label>
        -
        <label <?= $this->elements->setClassTodayClass($param['number'], 'id', 'stop_time') ?> class="stop_time">

        </label>
    </div>
    <div <?= $this->elements->setClassTodayClass($param['number'], 'id', 'pause-time') ?> class="pause-time">

    </div>
<?php } ?>