<div class="head-info">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h3>Вы ввошли как
                    <span class="badge badge-secondary btn btn-primary" data-toggle="modal" data-target="#exampleModalLong" >
                        <?= $auth['login'] ?>
                    </span>
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <p><strong>Отработано часов:</strong> <?= $auth['mouth_time'] ?> </p>
                <p><strong>Кол-во часов в месяц:</strong> <?= $params['work_time'] ?> ч</p>
                <p><strong>Опозданий на работу: </strong> <?= $auth['tardiness'] ?></p>
                <?= $this->tag->form(['index/index']) ?>
                    <div class="row">
                        <div class="col-6">
                            <?= $form->render('year', ['class' => 'form-control year', 'value' => $params['year']]) ?>
                        </div>
                        <div class="col-6">
                            <?= $form->render('mouth', ['class' => 'form-control mouth', 'value' => $params['mouth']]) ?>
                        </div>
                    </div>
                    <?= $form->render('submit', ['class' => 'd-none set-work']) ?>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="content-info">
    <table class="table table-bordered">
        <thead>
                <th><a href="#" class="hide-table">Скрыть/раскрыть</a></th>
                <th><?= $auth['name'] ?></th>
            <?php foreach ($users as $user) { ?>
                <th><?= $user['name'] ?></th>
            <?php } ?>
        </thead>
        <tbody>
             <?php foreach ($params['all_work_day'] as $param) { ?>
                 <tr <?= $this->elements->setClassTodayClass($param['number'], 'class', 'today') ?> class="hide">

                     <td class="text-center align-middle">
                         <?= $param['number'] ?>
                         <br />
                         <?= $param['day'] ?>
                     </td>

                     
                     <td class="text-center">
                         <?php if (isset($auth['auth_work'][$param['number']])) { ?>
    <div class="work-time">
        <label <?= $this->elements->setClassTodayClass($param['number'], 'id', 'start_time') ?> class="start_time">
                <?= $auth['auth_work'][$param['number']]['start_time'] ?>
        </label>
        -
        <label <?= $this->elements->setClassTodayClass($param['number'], 'id', 'stop_time') ?> class="stop_time">
            <?= $auth['auth_work'][$param['number']]['stop_time'] ?>
        </label>
    </div>
    <div <?= $this->elements->setClassTodayClass($param['number'], 'id', 'pause-time') ?> class="pause-time">
       <?php if (isset($auth['auth_work'][$param['number']]['pause'])) { ?>
             Пауза:  <?= $auth['auth_work'][$param['number']]['pause'] ?>
       <?php } ?>
    </div>
    <div class="day-work-time">
        <?php if (isset($auth['auth_work'][$param['number']]['all_time'])) { ?>
           Отработано:  <?= $auth['auth_work'][$param['number']]['all_time'] ?>
        <?php } ?>
    </div>
<?php } else { ?>
    <div class="work-time">

        <label <?= $this->elements->setClassTodayClass($param['number'], 'id', 'start_time') ?> class="start_time">

        </label>
        -
        <label <?= $this->elements->setClassTodayClass($param['number'], 'id', 'stop_time') ?> class="stop_time">

        </label>
    </div>
    <div <?= $this->elements->setClassTodayClass($param['number'], 'id', 'pause-time') ?> class="pause-time">

    </div>
<?php } ?>

                         <?= $this->elements->getButton($param['number']) ?>
                     </td>

                     
                     <?php foreach ($users as $user) { ?>
    <td class="text-center">
        <?php if (isset($user['work'][$param['number']])) { ?>
            <div class="work-time">
                <?= $user['work'][$param['number']]['start_time'] ?>
                -
                <?= $user['work'][$param['number']]['stop_time'] ?>
            </div>
            <div class="pause-time">
                <?php if (isset($user['work'][$param['number']]['pause'])) { ?>
                       Пауза: <?= $user['work'][$param['number']]['pause'] ?>
                <?php } ?>
            </div>
            <div class="day-work-time">
                <?php if (isset($user['work'][$param['number']]['day_time'])) { ?>
                    Отработано : <?= $user['work'][$param['number']]['day_time'] ?>
                <?php } ?>
            </div>
        <?php } ?>
    </td>
<?php } ?>
                 </tr>
             <?php } ?>
        </tbody>
    </table>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">
                    Инфо пользователя: <?= $auth['name'] ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label for="xample-text-input" class="col-4 col-form-label text-right"> Старый пароль</label>
                    <div class="col-8">
                        <input type="password" id='old-pass' class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="xample-text-input" class="col-4 col-form-label text-right"> Новый пароль</label>
                    <div class="col-8">
                        <input type="password" id="new-pass" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-primary change">Созранить</button>
            </div>
        </div>
    </div>
</div>


<?= $this->tag->javascriptInclude('/public/js/works.js') ?>
<?= $this->tag->javascriptInclude('/public/js/change.js') ?>

<?= $this->tag->javascriptInclude('/public/js/index/index.js') ?>