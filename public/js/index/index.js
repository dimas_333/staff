$(document).ready(function() {

    var index = {

        url : '/index/savetime',
        set_time : function(type)
        {
            $.post(index.url, {'type' : type}, function(data) {
                console.log(data);

                if(data['status'] == 'true') {
                    if(type == 'save'){

                        $("#start-work").attr('name', 'stop');
                        $("#start-work").removeClass("btn btn-success");
                        $("#start-work").addClass("btn btn-danger");
                        $("#start-work").text('Стоп');

                        if(typeof data['start_time'] != "undefined"){
                            $("#start_time").text(data['start_time']);
                        }

                        if(typeof data['pause'] != "undefined"){
                            $("#pause-time").text('Пауза: ' +data['pause']);
                            $("#stop_time").text('');
                        }

                    }
                    else if(type == 'stop') {
                        $("#start-work").attr('name', 'save');
                        $("#start-work").removeClass("btn btn-danger");
                        $("#start-work").addClass("btn btn-success");
                        $("#start-work").text('Старт');

                        if (typeof data['stop_time'] != "undefined") {
                            console.log('dsadsa');

                            $("#stop_time").text(data['stop_time']);
                        }

                    }
                }
                else{
                    alert('Произошла ошибка перезагрузите страницу');
                }
            }, 'Json');
        }
    }
    $('body').on('click', '#start-work', function() {
        index.set_time($(this).attr('name'));
    });
});