$(document).ready(function() {

    var weekend = {
             url: '/admin/setweekend',
        setWeekend: function (date, name, rep) {
            $.post(weekend.url, {'date' : date, 'name': name, 'rep' : rep}, function(data) {

                if(data['status'] == 'true'){
                    if(data['weekend']['rep'] == 'Y'){
                        var rep = 'Да';
                    }
                    else{
                        var rep = 'Нет';
                    }

                    var url = '/admin/weekenddelete/'+data['weekend']['id'];

                    $('.table-weekend').append('' +
                                                '<tr>' +
                                                    '<td>'+data['weekend']['date']+'</td>' +
                                                    '<td>'+data['weekend']['name']+'</td>' +
                                                    '<td>'+rep+'</td>' +
                                                    '<td><a href="'+url+'">Удалить</a></td>' +
                                                '</tr>');
                }
                else {
                    alert('Произошла ошибка');
                }

            }, 'Json');
        }
    }


/*    $('.day').hover(
        function() {
            $( this ).addClass( "active-day" );
        }, function() {
            $( this ).removeClass( "active-day" );
        }
    );*/

    $('.day').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus');
    });

    $('.add-weekend').on('click', function () {
        var date = $('#example-date-input').val();
        var name = $('#example-text-input').val();
        var rep = $('#example-text-select').val();

        if(date != ' '){
            weekend.setWeekend(date, name, rep);
        }

        $('#exampleModal').modal('hide');
    });

    $('.calendar-hide').on('click', function () {
        $('.table-calendar').toggle();
    });

    $('.weekends-hide').on('click', function () {
        $('.table-weekend').toggle();
    });
});