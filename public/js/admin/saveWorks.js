$(document).ready(function(){

    var saveWork = {
            url : '/admin/savetworktime',
        saveTime: function (type, year, mouth, param, id, val) {
            $.post(saveWork.url, {'type' : type, 'year' : year, 'mouth' : mouth, 'param' : param, 'id' : id, 'val': val}, function(data) {
                if(data['status' == 'false'])
                {
                    alert('Произошла ошибка перезагрузите страницу');
                }
            }, 'Json');
        }
    }



    $(".save-start-time").on("blur", function () {
        var year = $(".year").val();
        var mouth = $(".mouth").val();
        var param = JSON.parse($(this).attr('data-item'));
        var id = $(this).attr('name');
        var val = $(this).val();
        var type = 'start';

        saveWork.saveTime(type, year, mouth, param, id, val );

    });
    $(".save-stop-time").on("blur", function () {
        var year = $(".year").val();
        var mouth = $(".mouth").val();
        var param = JSON.parse($(this).attr('data-item'));
        var id = $(this).attr('name');
        var val = $(this).val();
        var type = 'stop';

        saveWork.saveTime(type, year, mouth, param, id, val );

    });
    $(".save-pause-time").on("blur", function () {
        var year = $(".year").val();
        var mouth = $(".mouth").val();
        var param = JSON.parse($(this).attr('data-item'));
        var id = $(this).attr('name');
        var val = $(this).val();
        var type = 'pause';

        saveWork.saveTime(type, year, mouth, param, id, val );

    });

    $('.save-tardiness-time').on('input', function () {
        var year = $(".year").val();
        var mouth = $(".mouth").val();
        var param = JSON.parse($(this).attr('data-item'));
        var id = $(this).attr('name');
        var val = $(this).attr('checked');
        var type = 'tardiness';

            if(val == 'checked'){
                val = null;
                $(this).removeAttr('checked');
            }
            else{
                $(this).attr('checked', true);
                val = 'Y';
            }

            saveWork.saveTime(type, year, mouth, param, id, val );
    })

    $('.user-filter').on('input', function () {
        var param = $(this).val();

        if(param == 'all') {
            $('.users').show();
        }else{
            $('.users').hide();
            $('.'+param).show();
        }

        alert(login);
    })
});