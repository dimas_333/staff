-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Сен 05 2018 г., 09:36
-- Версия сервера: 5.6.38
-- Версия PHP: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `training`
--

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `value` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`) VALUES
(1, 'project-name', 'Training'),
(2, 'work-time', '09:00'),
(3, 'start-work-time', '09:00');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `type` char(1) NOT NULL,
  `status` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `name`, `email`, `type`, `status`) VALUES
(1, 'admin', '$2y$08$eitjOWN2SldUaFlGWmxKcepjO24HAjrv2Dny017UsPeEuSyZ/xQNi', 'Олег', 'admin@local.ru', 'A', 'Y'),
(2, 'phalcon123', '$2y$08$c3pUSTA5QzJQZFZyM0R4VeiFUZcv53jeIIbmi5bh6.MkNh13Z3Hta', 'Максим', 'phalcon@loca.ru', 'U', 'Y'),
(3, 'Demo', '$2y$08$eS9HVVJDSldYcHhzeThpNuqAFKslI3Iqaq8laKX0qrKJQAHULw1nW', 'Павел', 'demo@local.ru', 'U', 'Y'),
(4, 'dimas', '$2y$08$TnFXQnYyWlhJTXJ0UkZEee1.WdEPRE2NAGQMFFjd5/X5fO0nwVshK', 'Дмитрий', 'dimas@local.ru', 'U', 'Y'),
(5, 'Oliver552', '$2y$08$cVh5alNFN1NBSDhMOFdsbeIlbGb40UUtTe2pdFXaE5gY8W5fiOmde', 'Пашка', 'local@mail.ru', 'U', 'Y');

-- --------------------------------------------------------

--
-- Структура таблицы `weekend`
--

CREATE TABLE `weekend` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `rep` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `weekend`
--

INSERT INTO `weekend` (`id`, `date`, `name`, `rep`) VALUES
(2, '2018-08-31', 'День независимости', 'N'),
(9, '2017-09-20', 'майн', 'Y');

-- --------------------------------------------------------

--
-- Структура таблицы `works`
--

CREATE TABLE `works` (
  `id` int(11) NOT NULL,
  `day` date NOT NULL,
  `start_time` time DEFAULT NULL,
  `stop_time` time DEFAULT NULL,
  `pause` time DEFAULT NULL,
  `tardiness` char(1) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `works`
--

INSERT INTO `works` (`id`, `day`, `start_time`, `stop_time`, `pause`, `tardiness`, `user_id`) VALUES
(21, '2018-08-01', '08:20:00', '16:00:00', '02:00:00', NULL, 2),
(22, '2018-08-01', '09:00:00', '17:00:00', '01:00:00', NULL, 3),
(23, '2018-08-02', '08:00:00', '17:00:00', '00:00:00', NULL, 2),
(24, '2018-09-01', '12:55:00', '16:00:00', '00:00:00', 'Y', 2),
(25, '2018-09-01', '10:00:00', '17:00:00', '00:00:00', 'Y', 3),
(27, '2018-09-02', '12:26:41', '16:00:00', '01:36:40', 'Y', 3),
(29, '2018-09-01', '06:00:00', '17:00:00', '00:00:00', 'Y', 5),
(30, '2018-09-03', '06:58:20', '15:17:25', '03:40:28', NULL, 3),
(31, '2018-09-02', '08:00:00', '16:58:00', '00:00:00', NULL, 2),
(32, '2018-09-02', '07:00:00', '18:00:00', '01:53:00', NULL, 4),
(33, '2018-09-03', '09:00:00', '18:00:00', NULL, NULL, 2),
(34, '2018-09-01', '07:00:00', '16:00:00', '00:00:00', NULL, 4),
(35, '2018-09-03', '09:00:00', '18:00:00', NULL, NULL, 4),
(36, '2018-09-03', '08:00:00', NULL, NULL, NULL, 5),
(47, '2018-09-04', '09:51:27', '09:52:25', NULL, NULL, 2),
(50, '2018-09-04', '10:03:30', NULL, '00:13:04', NULL, 1),
(52, '2018-09-04', '10:19:03', NULL, NULL, 'Y', 4),
(53, '2018-09-01', '11:00:00', '19:00:00', '00:00:00', NULL, 1),
(54, '2018-09-02', '00:00:00', '00:00:00', '00:00:00', NULL, 1),
(55, '2018-09-04', '15:05:24', '00:05:24', '01:13:41', 'Y', 3),
(56, '2018-09-05', '09:34:00', '09:34:18', '00:00:08', 'Y', 3);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `weekend`
--
ALTER TABLE `weekend`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `works`
--
ALTER TABLE `works`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `weekend`
--
ALTER TABLE `weekend`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `works`
--
ALTER TABLE `works`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `works`
--
ALTER TABLE `works`
  ADD CONSTRAINT `works_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
